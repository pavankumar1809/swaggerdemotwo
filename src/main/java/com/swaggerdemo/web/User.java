package com.swaggerdemo.web;

import io.swagger.annotations.ApiModelProperty;

public class User {

	@ApiModelProperty(notes = "The database generated product ID")
	private long id;
	@ApiModelProperty(value = "full name required", example = "Pavan kumar K",notes = "name of the user", required = true, readOnly=true)
	private String name;
	@ApiModelProperty(value = "age must be greater than 18", example = "25", notes = "age of the user", required = true)
	private int age;
	@ApiModelProperty(notes = "salary of the user", hidden = true)
	private double salary;
	@ApiModelProperty(notes = "location of the user",example = "Ahmedabad", name = "place")
	private String location;

	public User() {
		id = 0;
	}

	public User(long id, String name, int age, double salary) {
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + "]";
	}

}